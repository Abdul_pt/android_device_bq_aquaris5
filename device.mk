$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# The gps config appropriate for this device
$(call inherit-product, device/common/gps/gps_us_supl.mk)

$(call inherit-product-if-exists, vendor/bq/aquaris5/aquaris5-vendor.mk)

DEVICE_FOLDER := device/bq/aquaris5
PRODUCT_AAPT_CONFIG += normal large hdpi
PRODUCT_AAPT_PREF_CONFIG := mdpi
PRODUCT_TAGS += dalvik.gc.type-precise
DEVICE_PACKAGE_OVERLAYS += $(DEVICE_FOLDER)/overlay


# Set default USB interface
PRODUCT_PROPERTY_OVERRIDES += \
        service.adb.root=1 \
        ro.secure=0 \
        ro.allow.mock.location=1 \
        ro.debuggable=1 \
        persist.sys.usb.config=mtp

$(call inherit-product, frameworks/native/build/phone-hdpi-dalvik-heap.mk)
$(call inherit-product, build/target/product/full.mk)


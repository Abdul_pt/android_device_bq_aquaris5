# Boot animation
TARGET_SCREEN_HEIGHT := 960
TARGET_SCREEN_WIDTH := 540

# Inherit some common CyanogenMod stuff
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Inherit device configuration
$(call inherit-product, device/bq/aquaris5/full_aquaris5.mk)

# Device identifier. This must come after all inclusions
PRODUCT_NAME := cm_aquaris5
# Set product name
PRODUCT_BUILD_PROP_OVERRIDES += \
                       PRODUCT_NAME=Aquaris5 \
                       BUILD_UTC_DATE=0
